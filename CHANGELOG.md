# Changelog

## 1.0.0

### Changed
- Compatible with Symfony 7.x.

## 0.3.0

### Added
- Added force dice for the Star Wars Dice roller.
- Added getter methods for Genesys/Star Wars auto symbols.

## 0.2.1

### Added
- Added getter methods for dice types.

## 0.2.0

### Added
- Assets for the Star Wars dice. For now it uses the Genesys DiceRoller class.
- Added a new DiceRoller class for X-Wing.

### Changed
- Fixed typo in the README file.

## 0.1.0

### Added
- Genesys DiceRoller can now take auto symbols for failures, successes, threats, and advantages. I.e. in case your dice roll has a bonus advantage to be included in the roll.

## 0.0.2

### Changed
- DiceRoller getResultSymbols method can now return net result.

## 0.0.1

### Added
- Initial development.
