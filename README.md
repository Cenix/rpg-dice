Cenix/RpgDice: A Role Playing Game Dice Roller
===============================
Roll dice and parse results for RPGs and other dice based games.

Currently supports the following games:
* [Genesys](https://edge-studio.net/categories-games/genesys/) narrative RPG system by FFG/Edge Studio.
* [Star Wars](https://edge-studio.net/categories-games/starwarsrpg/) narrative RPG system by FFG/Edge Studio.
* [X-wing](https://www.fantasyflightgames.com/en/products/x-wing-second-edition/) tactical miniatures game.
* Any system using regular polyhedral dice (D4, D6, D8, D10, D12, D20, D100)


<a name="requirements"></a>
# Requirements

```json
"php": "^8.1",
"symfony/form": "^7.1"
```

For more information, see the [`composer.json`](composer.json) file.

# Installation

Via [Composer](https://getcomposer.org/) (https://packagist.org/packages/cenix/rpg-dice):

    composer require cenix/rpg-dice

Via Bitbucket:

    git clone git@bitbucket.org:Cenix/rpg-dice.git

## Install Assets
### Webpack Encore
in webpack.config.js add the following:
```
...
Encore
    ...
    .copyFiles([
        ...
        {from: './vendor/cenix/rpg-dice/assets', to: 'dice/[path][name].[ext]'}
    ])
```

# Tests

# License & Disclaimer

See [`LICENSE`](LICENSE) file. Basically: Use this library at your own risk.

# Contributing

I prefer that you create a ticket and or a pull request at https://bitbucket.org/Cenix/rpg-dice/, and have a discussion about a feature or bug here.

# Credits

## Authors

- **Christian Nikolajsen** (cenix)<br>E-mail: <a href="mailto:christian@cenix.dk">christian@cenix.dk</a><br>Homepage: <a href="https://bitbucket.org/Cenix/">https://bitbucket.org/Cenix/</a>
