<?php

declare(strict_types=1);

namespace Cenix\RpgDice;

use Exception;

interface DiceInterface
{
    /**
     * Roll a specific dice type a defined number of times storing the results in public properties of the dice class.
     *
     * @param int $rolls The number of times to roll the dice.
     * @param int|null $faceOverride Allows "fudging" the dice setting the result to a specific dice face.
     *
     * @throws Exception
     */
    public function roll(int $rolls = 1, ?int $faceOverride = null): void;

    /**
     * @return array<int|string>
     */
    public function getRolledFaces(): array;

    /**
     * @param array<int|string> $rolledFace
     */
    public function setRolledFaces(array $rolledFace): void;
}
