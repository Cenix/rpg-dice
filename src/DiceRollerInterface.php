<?php

declare(strict_types=1);

namespace Cenix\RpgDice;

interface DiceRollerInterface
{
//    public function rollDicePool(DiceRoller $diceRoller): void;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getSuccesses(string $variant = 'gross'): int;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getAdvantages(string $variant = 'gross'): int;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getTriumphs(string $variant = 'gross'): int;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getThreats(string $variant = 'gross'): int;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getFailures(string $variant = 'gross'): int;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getDespairs(string $variant = 'gross'): int;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getBlanks(string $variant = 'gross'): int;

    public function setAutoAdvantage(int $autoAdvantage): void;

    public function setAutoThreat(int $autoThreat): void;

    public function setAutoSuccess(int $autoSuccess): void;

    public function setAutoFail(int $autoFail): void;

    /**
     * @param string $variant Variant of the result; 'gross' for the full result or 'net' for the deducted result.
     */
    public function getResultText(string $variant = 'gross'): string;

    /**
     * @return array<int|string>
     */
    public function getRolledFaces(): array;

    /**
     * @return array<int|string>
     */
    public function getAutoSymbols(): array;

    /**
     * @param array<int|string> $rolledFaces
     */
    public function setRolledFaces(array $rolledFaces): void;

    /**
     * @return string[]
     */
    public function getResultSymbols(string $variant = 'gross'): array;
}
