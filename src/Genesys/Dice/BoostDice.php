<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class BoostDice implements DiceInterface
{
    public const NAME = 'boost';
    private const FACES = 6;
    private const FACE_1 = ['blank' => 1, 'face-icon-name' => 'boost-blank'];
    private const FACE_2 = ['blank' => 1, 'face-icon-name' => 'boost-blank'];
    private const FACE_3 = ['advantage' => 1, 'face-icon-name' => 'boost-advantage'];
    private const FACE_4 = ['success' => 1, 'face-icon-name' => 'boost-success'];
    private const FACE_5 = ['success' => 1, 'advantage' => 1, 'face-icon-name' => 'boost-success-advantage'];
    private const FACE_6 = ['advantage' => 2, 'face-icon-name' => 'boost-advantage-advantage'];

    public int $resultSuccesses = 0;
    public int $resultAdvantages = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'success':
                        $this->resultSuccesses += (int)$value;
                        break;

                    case 'advantage':
                        $this->resultAdvantages += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
