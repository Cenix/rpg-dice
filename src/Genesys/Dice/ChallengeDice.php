<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class ChallengeDice implements DiceInterface
{
    public const NAME = 'challenge';
    public const FACES = 12;
    private const FACE_1 = ['blank' => 1, 'face-icon-name' => 'challenge-blank'];
    private const FACE_2 = ['threat' => 1, 'face-icon-name' => 'challenge-threat'];
    private const FACE_3 = ['threat' => 1, 'face-icon-name' => 'challenge-threat'];
    private const FACE_4 = ['despair' => 1, 'face-icon-name' => 'challenge-despair'];
    private const FACE_5 = ['failure' => 1, 'face-icon-name' => 'challenge-failure'];
    private const FACE_6 = ['failure' => 1, 'face-icon-name' => 'challenge-failure'];
    private const FACE_7 = ['threat' => 2, 'face-icon-name' => 'challenge-threat-threat'];
    private const FACE_8 = ['threat' => 2, 'face-icon-name' => 'challenge-threat-threat'];
    private const FACE_9 = ['failure' => 1, 'threat' => 1, 'face-icon-name' => 'challenge-failure-threat'];
    private const FACE_10 = ['failure' => 1, 'threat' => 1, 'face-icon-name' => 'challenge-failure-threat'];
    private const FACE_11 = ['failure' => 2, 'face-icon-name' => 'challenge-failure-failure'];
    private const FACE_12 = ['failure' => 2, 'face-icon-name' => 'challenge-failure-failure'];

    public int $resultThreats = 0;
    public int $resultFailures = 0;
    public int $resultDespairs = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'threat':
                        $this->resultThreats += (int)$value;
                        break;

                    case 'failure':
                        $this->resultFailures += (int)$value;
                        break;
                    case 'despair':
                        $this->resultDespairs += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
