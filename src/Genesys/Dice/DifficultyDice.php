<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class DifficultyDice implements DiceInterface
{
    public const NAME = 'difficulty';
    public const FACES = 8;
    public const FACE_1 = ['blank' => 1, 'face-icon-name' => 'difficulty-blank'];
    public const FACE_2 = ['failure' => 2, 'face-icon-name' => 'difficulty-failure-failure'];
    public const FACE_3 = ['threat' => 2, 'face-icon-name' => 'difficulty-threat-threat'];
    public const FACE_4 = ['failure' => 1, 'threat' => 1, 'face-icon-name' => 'difficulty-failure-threat'];
    public const FACE_5 = ['failure' => 1, 'face-icon-name' => 'difficulty-failure'];
    public const FACE_6 = ['threat' => 1, 'face-icon-name' => 'difficulty-threat'];
    public const FACE_7 = ['threat' => 1, 'face-icon-name' => 'difficulty-threat'];
    public const FACE_8 = ['threat' => 1, 'face-icon-name' => 'difficulty-threat'];

    public int $resultThreats = 0;
    public int $resultFailures = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'threat':
                        $this->resultThreats += (int)$value;
                        break;

                    case 'failure':
                        $this->resultFailures += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
