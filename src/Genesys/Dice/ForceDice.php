<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class ForceDice implements DiceInterface
{
    public const NAME = 'force';
    public const FACES = 12;
    private const FACE_1 = ['black' => 1, 'face-icon-name' => 'force-black'];
    private const FACE_2 = ['black' => 1, 'face-icon-name' => 'force-black'];
    private const FACE_3 = ['black' => 1, 'face-icon-name' => 'force-black'];
    private const FACE_4 = ['black' => 1, 'face-icon-name' => 'force-black'];
    private const FACE_5 = ['black' => 1, 'face-icon-name' => 'force-black'];
    private const FACE_6 = ['black' => 1, 'face-icon-name' => 'force-black'];
    private const FACE_7 = ['white' => 1, 'face-icon-name' => 'force-white'];
    private const FACE_8 = ['white' => 1, 'face-icon-name' => 'force-white'];
    private const FACE_9 = ['white' => 2, 'face-icon-name' => 'force-white-white'];
    private const FACE_10 = ['white' => 2, 'face-icon-name' => 'force-white-white'];
    private const FACE_11 = ['white' => 2, 'face-icon-name' => 'force-white-white'];
    private const FACE_12 = ['black' => 2, 'face-icon-name' => 'force-black-black'];

    public int $resultWhites = 0;
    public int $resultBlacks = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'white':
                        $this->resultWhites += (int)$value;
                        break;

                    case 'black':
                        $this->resultBlacks += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
