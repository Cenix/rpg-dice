<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class ProficiencyDice implements DiceInterface
{
    public const NAME = 'proficiency';
    public const FACES = 12;

    private const FACE_1 = ['blank' => 1, 'face-icon-name' => 'proficiency-blank'];
    private const FACE_2 = ['success' => 2, 'face-icon-name' => 'proficiency-success-success'];
    private const FACE_3 = ['success' => 2, 'face-icon-name' => 'proficiency-success-success'];
    private const FACE_4 = ['success' => 1, 'advantage' => 1, 'face-icon-name' => 'proficiency-success-advantage'];
    private const FACE_5 = ['success' => 1, 'advantage' => 1, 'face-icon-name' => 'proficiency-success-advantage'];
    private const FACE_6 = ['success' => 1, 'advantage' => 1, 'face-icon-name' => 'proficiency-success-advantage'];
    private const FACE_7 = ['advantage' => 2, 'face-icon-name' => 'proficiency-advantage-advantage'];
    private const FACE_8 = ['advantage' => 2, 'face-icon-name' => 'proficiency-advantage-advantage'];
    private const FACE_9 = ['triumph' => 1, 'face-icon-name' => 'proficiency-triumph'];
    private const FACE_10 = ['success' => 1, 'face-icon-name' => 'proficiency-success'];
    private const FACE_11 = ['success' => 1, 'face-icon-name' => 'proficiency-success'];
    private const FACE_12 = ['advantage' => 1, 'face-icon-name' => 'proficiency-advantage'];

    public int $resultSuccesses = 0;
    public int $resultAdvantages = 0;
    public int $resultTriumphs = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace  */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'success':
                        $this->resultSuccesses += (int)$value;
                        break;

                    case 'advantage':
                        $this->resultAdvantages += (int)$value;
                        break;

                    case 'triumph':
                        $this->resultTriumphs += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
