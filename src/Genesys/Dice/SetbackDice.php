<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class SetbackDice implements DiceInterface
{
    public const NAME = 'setback';
    public const FACES = 6;
    public const FACE_1 = ['blank' => 1, 'face-icon-name' => 'setback-blank'];
    public const FACE_2 = ['blank' => 1, 'face-icon-name' => 'setback-blank'];
    public const FACE_3 = ['threat' => 1, 'face-icon-name' => 'setback-threat'];
    public const FACE_4 = ['threat' => 1, 'face-icon-name' => 'setback-threat'];
    public const FACE_5 = ['failure' => 1, 'face-icon-name' => 'setback-failure'];
    public const FACE_6 = ['failure' => 1, 'face-icon-name' => 'setback-failure'];

    public int $resultThreats = 0;
    public int $resultFailures = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'threat':
                        $this->resultThreats += (int)$value;
                        break;

                    case 'failure':
                        $this->resultFailures += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
