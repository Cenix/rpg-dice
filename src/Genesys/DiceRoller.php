<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys;

use Cenix\RpgDice\DiceRollerInterface;
use Cenix\RpgDice\Genesys\Dice\AbilityDice;
use Cenix\RpgDice\Genesys\Dice\BoostDice;
use Cenix\RpgDice\Genesys\Dice\ChallengeDice;
use Cenix\RpgDice\Genesys\Dice\DifficultyDice;
use Cenix\RpgDice\Genesys\Dice\ForceDice;
use Cenix\RpgDice\Genesys\Dice\ProficiencyDice;
use Cenix\RpgDice\Genesys\Dice\SetbackDice;
use Exception;
use Symfony\Component\String\Inflector\EnglishInflector;

use function array_merge;
use function count;
use function implode;
use function lcfirst;
use function max;
use function property_exists;
use function sprintf;
use function ucfirst;

class DiceRoller implements DiceRollerInterface
{
    private const SYSTEM_DICE = [
        'ability',
        'proficiency',
        'boost',
        'difficulty',
        'challenge',
        'setback',
        'force',
    ];

    private string $variantSystem = 'genesys';

    public int $ability = 0;
    public int $proficiency = 0;
    public int $boost = 0;
    public int $difficulty = 0;
    public int $challenge = 0;
    public int $setback = 0;
    public int $force = 0;

    public int $autoAdvantages = 0;
    public int $autoThreats = 0;
    public int $autoSuccesses = 0;
    public int $autoFailures = 0;

    private int $resultSuccesses = 0;
    private int $resultAdvantages = 0;
    private int $resultTriumphs = 0;
    private int $resultThreats = 0;
    private int $resultFailures = 0;
    private int $resultDespairs = 0;
    private int $resultBlanks = 0;
    private int $resultWhites = 0;
    private int $resultBlacks = 0;

    private AbilityDice $abilityDice;
    private BoostDice $boostDice;
    private ChallengeDice $challengeDice;
    private DifficultyDice $difficultyDice;
    private ProficiencyDice $proficiencyDice;
    private SetbackDice $setbackDice;
    private ForceDice $forceDice;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function __construct()
    {
        $this->abilityDice = new AbilityDice();
        $this->boostDice = new BoostDice();
        $this->challengeDice = new ChallengeDice();
        $this->difficultyDice = new DifficultyDice();
        $this->proficiencyDice = new ProficiencyDice();
        $this->setbackDice = new SetbackDice();
        $this->forceDice = new ForceDice();
    }

    /**
     * @throws Exception
     */
    public function rollDicePool(): void
    {
        foreach (self::SYSTEM_DICE as $diceType) {
            $dice = $this->{lcfirst($diceType) . 'Dice'};
            $dice->roll($this->{$diceType});

            $this->setRolledFaces($dice->getRolledFaces());

            foreach (
                [
                    'successes',
                    'advantages',
                    'triumphs',
                    'failures',
                    'threats',
                    'despairs',
                    'whites',
                    'blacks'
                ] as $resultType
            ) {
                if (property_exists($dice, 'result' . ucfirst($resultType))) {
                    $this->{'result' . ucfirst($resultType)} += $dice->{'result' . ucfirst($resultType)};
                }
            }
        }
    }

    public function getSuccesses(string $variant = 'gross'): int
    {
        $result = $this->resultSuccesses + $this->autoSuccesses;

        if ($variant === 'net') {
            return max(
                0,
                ($result + $this->getTriumphs()) - ($this->getFailures() + $this->getDespairs())
            );
        }

        return $result;
    }

    public function getAdvantages(string $variant = 'gross'): int
    {
        $result = $this->resultAdvantages + $this->autoAdvantages;

        if ($variant === 'net') {
            return max(
                0,
                $result - $this->getThreats()
            );
        }

        return $result;
    }

    public function getTriumphs(string $variant = 'gross'): int
    {
        return $this->resultTriumphs;
    }

    public function getThreats(string $variant = 'gross'): int
    {
        $result = $this->resultThreats + $this->autoThreats;

        if ($variant === 'net') {
            return max(
                0,
                $result - $this->getAdvantages()
            );
        }

        return $result;
    }

    public function getFailures(string $variant = 'gross'): int
    {
        $result = $this->resultFailures + $this->autoFailures;

        if ($variant === 'net') {
            return max(
                0,
                ($result + $this->getDespairs()) - ($this->getSuccesses() + $this->getTriumphs())
            );
        }

        return $this->resultFailures;
    }

    public function getDespairs(string $variant = 'gross'): int
    {
        return $this->resultDespairs;
    }

    public function getBlanks(string $variant = 'gross'): int
    {
        return $this->resultBlanks;
    }

    public function getWhites(string $variant = 'gross'): int
    {
        return $this->resultWhites;
    }

    public function getBlacks(string $variant = 'gross'): int
    {
        return $this->resultBlacks;
    }

    public function setAbility(int $ability): void
    {
        $this->ability = $ability;
    }

    public function getAbility(): int
    {
        return $this->ability;
    }

    public function setProficiency(int $proficiency): void
    {
        $this->proficiency = $proficiency;
    }

    public function getProficiency(): int
    {
        return $this->proficiency;
    }

    public function setBoost(int $boost): void
    {
        $this->boost = $boost;
    }

    public function getBoost(): int
    {
        return $this->boost;
    }

    public function setDifficulty(int $difficulty): void
    {
        $this->difficulty = $difficulty;
    }

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function setChallenge(int $challenge): void
    {
        $this->challenge = $challenge;
    }

    public function getChallenge(): int
    {
        return $this->challenge;
    }

    public function setSetback(int $setback): void
    {
        $this->setback = $setback;
    }

    public function getSetback(): int
    {
        return $this->setback;
    }

    public function setForce(int $force): void
    {
        $this->force = $force;
    }

    public function getForce(): int
    {
        return $this->force;
    }

    public function setAutoAdvantage(int $autoAdvantage): void
    {
        $this->autoAdvantages = $autoAdvantage;
    }

    public function setAutoThreat(int $autoThreat): void
    {
        $this->autoThreats = $autoThreat;
    }

    public function setAutoSuccess(int $autoSuccess): void
    {
        $this->autoSuccesses = $autoSuccess;
    }

    public function setAutoFail(int $autoFail): void
    {
        $this->autoFailures = $autoFail;
    }

    public function getAutoAdvantage(): int
    {
        return $this->autoAdvantages;
    }

    public function getAutoThreat(): int
    {
        return $this->autoThreats;
    }

    public function getAutoSuccess(): int
    {
        return $this->autoSuccesses;
    }

    public function getAutoFail(): int
    {
        return $this->autoFailures;
    }

    public function getResultText(string $variant = 'gross'): string
    {
        $inflector = new EnglishInflector();

        $resultTextParts = [];

        foreach (['success', 'advantage', 'triumph', 'failure', 'threat', 'despair', 'white', 'black'] as $resultType) {
            $pluralizedResultType = $inflector->pluralize($resultType)[0];
            $resultScore = $this->{'get' . ucfirst($pluralizedResultType)}($variant);

            if ($resultScore > 0) {
                if (in_array($resultType, ['black', 'white'])) {
                    $resultTextParts[] = sprintf(
                        '%d %s force %s',
                        $resultScore,
                        $resultType,
                        $resultScore > 1 ? 'pips' : 'pip'
                    );
                } else {
                    $resultTextParts[] = sprintf(
                        '%d %s',
                        $resultScore,
                        $resultScore > 1 ? $pluralizedResultType : $resultType
                    );
                }
            }
        }

        if (count($resultTextParts) > 0) {
            return implode(', ', $resultTextParts);
        }

        return '*Wash*';
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFaces): void
    {
        $this->rolledFaces = array_merge($this->rolledFaces, $rolledFaces);
    }

    /**
     * @inheritdoc
     */
    public function getAutoSymbols(): array
    {
        $inflector = new EnglishInflector();

        $symbols = [];

        foreach (['success', 'advantage', 'failure', 'threat'] as $resultType) {
            $pluralizedResultType = $inflector->pluralize($resultType)[0];
            $autoScore = $this->{'auto' . ucfirst($pluralizedResultType)};

            for ($i = 0; $i < $autoScore; $i++) {
                $symbols[] = $resultType;
            }
        }

        return $symbols;
    }

    /**
     * @inheritdoc
     */
    public function getResultSymbols(string $variant = 'gross'): array
    {
        $inflector = new EnglishInflector();

        $symbols = [];
        foreach (['success', 'advantage', 'triumph', 'failure', 'threat', 'despair', 'white', 'black'] as $resultType) {
            $pluralizedResultType = $inflector->pluralize($resultType)[0];
            $resultScore = $this->{'get' . ucfirst($pluralizedResultType)}($variant);

            for ($i = 0; $i < $resultScore; $i++) {
                $symbols[] = $resultType;
            }
        }

        return $symbols;
    }

    public function getSystemDice(): array
    {
        return self::SYSTEM_DICE;
    }

    /**
     * @return string
     */
    public function getVariantSystem(): string
    {
        return $this->variantSystem;
    }

    /**
     * @param string $variantSystem
     */
    public function setVariantSystem(string $variantSystem): void
    {
        $this->variantSystem = $variantSystem;
    }
}
