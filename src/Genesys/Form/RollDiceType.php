<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Genesys\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RollDiceType extends AbstractType
{
    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $rangeFieldOptions = [
            'attr' => [
                'class' => 'range-slider__range',
                'min' => 0,
                'max' => 5
            ],
            'help' => '0',
            'help_attr' => [
                'class' => 'range-slider__value'
            ],
            'row_attr' => ['class' => 'range-slider mb-3 p-2', 'data-action' => 'change->dice#selectDice'],
        ];

        $builder
            ->add('ability', RangeType::class, $rangeFieldOptions)
            ->add('proficiency', RangeType::class, $rangeFieldOptions)
            ->add('boost', RangeType::class, $rangeFieldOptions)
            ->add('difficulty', RangeType::class, $rangeFieldOptions)
            ->add('challenge', RangeType::class, $rangeFieldOptions)
            ->add('setback', RangeType::class, $rangeFieldOptions);

        if ($options['data']->getVariantSystem() === 'starwars') {
            $builder
                ->add('force', RangeType::class, $rangeFieldOptions);
        }

        $builder
            ->add('autoAdvantages', RangeType::class, $rangeFieldOptions)
            ->add('autoThreats', RangeType::class, $rangeFieldOptions)
            ->add('autoSuccesses', RangeType::class, $rangeFieldOptions)
            ->add('autoFailures', RangeType::class, $rangeFieldOptions);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
