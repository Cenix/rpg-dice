<?php

declare(strict_types=1);

namespace Cenix\RpgDice\PHPStan;

use PHPStan\Reflection\ConstantReflection;
use PHPStan\Rules\Constants\AlwaysUsedClassConstantsExtension;

use function in_array;

class ConstantsExtension implements AlwaysUsedClassConstantsExtension
{
    public function isAlwaysUsed(ConstantReflection $constant): bool
    {
        $privateConstants = [
            'FACE_1',
            'FACE_2',
            'FACE_3',
            'FACE_4',
            'FACE_5',
            'FACE_6',
            'FACE_7',
            'FACE_8',
            'FACE_9',
            'FACE_10',
            'FACE_11',
            'FACE_12',
        ];

        if (in_array($constant->getName(), $privateConstants)) {
            return true;
        }

        return false;
    }
}
