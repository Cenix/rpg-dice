<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Xwing\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class AttackDice implements DiceInterface
{
    public const NAME = 'attack';
    public const FACES = 8;
    public const FACE_1 = ['blank' => 1, 'face-icon-name' => 'attack-blank'];
    public const FACE_2 = ['blank' => 1, 'face-icon-name' => 'attack-blank'];
    public const FACE_3 = ['critical' => 1, 'face-icon-name' => 'attack-critical'];
    public const FACE_4 = ['hit' => 1, 'face-icon-name' => 'attack-hit'];
    public const FACE_5 = ['hit' => 1, 'face-icon-name' => 'attack-hit'];
    public const FACE_6 = ['hit' => 1, 'face-icon-name' => 'attack-hit'];
    public const FACE_7 = ['focus' => 1, 'face-icon-name' => 'attack-focus'];
    public const FACE_8 = ['focus' => 1, 'face-icon-name' => 'attack-focus'];

    public int $resultCriticals = 0;
    public int $resultHits = 0;
    public int $resultFocuses = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'critical':
                        $this->resultCriticals += (int)$value;
                        break;

                    case 'hit':
                        $this->resultHits += (int)$value;
                        break;

                    case 'focus':
                        $this->resultFocuses += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
