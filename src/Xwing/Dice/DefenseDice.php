<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Xwing\Dice;

use Cenix\RpgDice\DiceInterface;

use function constant;
use function random_int;

class DefenseDice implements DiceInterface
{
    public const NAME = 'defense';
    public const FACES = 8;
    public const FACE_1 = ['blank' => 1, 'face-icon-name' => 'defense-blank'];
    public const FACE_2 = ['blank' => 1, 'face-icon-name' => 'defense-blank'];
    public const FACE_3 = ['blank' => 1, 'face-icon-name' => 'defense-blank'];
    public const FACE_4 = ['focus' => 1, 'face-icon-name' => 'defense-focus'];
    public const FACE_5 = ['focus' => 1, 'face-icon-name' => 'defense-focus'];
    public const FACE_6 = ['evade' => 1, 'face-icon-name' => 'defense-evade'];
    public const FACE_7 = ['evade' => 1, 'face-icon-name' => 'defense-evade'];
    public const FACE_8 = ['evade' => 1, 'face-icon-name' => 'defense-evade'];

    public int $resultEvades = 0;
    public int $resultFocuses = 0;
    public int $resultBlanks = 0;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function roll(int $rolls = 1, ?int $faceOverride = null): void
    {
        for ($roll = 1; $roll <= $rolls; $roll++) {
            $face = $faceOverride ?? random_int(1, self::FACES);
            /** @var array|int[]|string[] $resultFace */
            $resultFace = constant('self::FACE_' . $face);

            $this->setRolledFaces($resultFace);

            foreach ($resultFace as $result => $value) {
                switch ($result) {
                    case 'blank':
                        $this->resultBlanks += (int)$value;
                        break;

                    case 'evade':
                        $this->resultEvades += (int)$value;
                        break;

                    case 'focus':
                        $this->resultFocuses += (int)$value;
                        break;
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFace): void
    {
        $this->rolledFaces[] = $rolledFace['face-icon-name'];
    }
}
