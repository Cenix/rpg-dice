<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Xwing;

use Cenix\RpgDice\Xwing\Dice\AttackDice;
use Cenix\RpgDice\Xwing\Dice\DefenseDice;
use Exception;
use Symfony\Component\String\Inflector\EnglishInflector;

use function array_merge;
use function count;
use function implode;
use function lcfirst;
use function max;
use function property_exists;
use function sprintf;
use function ucfirst;

class DiceRoller
{
    private const SYSTEM_DICE = [
        'attack',
        'defense',
    ];

    public int $attack = 0;
    public int $defense = 0;

    private int $resultCrits = 0;
    private int $resultHits = 0;
    private int $resultEvades = 0;
    private int $resultFocuses = 0;
    private int $resultBlanks = 0;

    private AttackDice $attackDice;
    private DefenseDice $defenseDice;

    /** @var array<int|string> */
    private array $rolledFaces = [];

    public function __construct()
    {
        $this->attackDice = new AttackDice();
        $this->defenseDice = new DefenseDice();
    }

    /**
     * @throws Exception
     */
    public function rollDicePool(): void
    {
        foreach (self::SYSTEM_DICE as $diceType) {
            $dice = $this->{lcfirst($diceType) . 'Dice'};
            $dice->roll($this->{$diceType});

            $this->setRolledFaces($dice->getRolledFaces());

            foreach (['crits', 'hits', 'evades', 'focuses'] as $resultType) {
                if (property_exists($dice, 'result' . ucfirst($resultType))) {
                    $this->{'result' . ucfirst($resultType)} += $dice->{'result' . ucfirst($resultType)};
                }
            }
        }
    }

    public function getCrits(string $variant = 'gross'): int
    {
        $result = $this->resultCrits;

        if ($variant === 'net') {
            return max(
                0,
                ($result + 0) - (0)
            );
        }

        return $result;
    }

    public function getHits(string $variant = 'gross'): int
    {
        $result = $this->resultHits;

        if ($variant === 'net') {
            return max(
                0,
                $result - 0
            );
        }

        return $result;
    }

    public function getFocuses(string $variant = 'gross'): int
    {
        return $this->resultFocuses;
    }

    public function getBlanks(string $variant = 'gross'): int
    {
        return $this->resultBlanks;
    }

    public function setAttack(int $attack): void
    {
        $this->attack = $attack;
    }

    public function setDefense(int $defense): void
    {
        $this->defense = $defense;
    }

    public function getAttack(): int
    {
        return $this->attack;
    }

    public function getDefense(): int
    {
        return $this->defense;
    }

    public function getResultText(string $variant = 'gross'): string
    {
        $inflector = new EnglishInflector();

        $resultTextParts = [];

        foreach (['crit', 'hit', 'focus'] as $resultType) {
            $pluralizedResultType = $inflector->pluralize($resultType)[0];
            $resultScore = $this->{'get' . ucfirst($pluralizedResultType)}($variant);

            if ($resultScore > 0) {
                $resultTextParts[] = sprintf(
                    '%d %s',
                    $resultScore,
                    $resultScore > 1 ? $pluralizedResultType : $resultType
                );
            }
        }

        if (count($resultTextParts) > 0) {
            return implode(', ', $resultTextParts);
        }

        return '*Wash*';
    }

    /**
     * @inheritdoc
     */
    public function getRolledFaces(): array
    {
        return $this->rolledFaces;
    }

    /**
     * @inheritdoc
     */
    public function setRolledFaces(array $rolledFaces): void
    {
        $this->rolledFaces = array_merge($this->rolledFaces, $rolledFaces);
    }

    /**
     * @inheritdoc
     */
    public function getResultSymbols(string $variant = 'gross'): array
    {
        $inflector = new EnglishInflector();

        $symbols = [];
        foreach (['crit', 'hit', 'focus'] as $resultType) {
            $pluralizedResultType = $inflector->pluralize($resultType)[0];
            $resultScore = $this->{'get' . ucfirst($pluralizedResultType)}($variant);

            for ($i = 0; $i < $resultScore; $i++) {
                $symbols[] = $resultType;
            }
        }

        return $symbols;
    }

    /**
     * @inheritdoc
     */
    public function getAutoSymbols(): array
    {
        $symbols = [];

        return $symbols;
    }

    public function getSystemDice(): array
    {
        return self::SYSTEM_DICE;
    }
}
