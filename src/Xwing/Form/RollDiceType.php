<?php

declare(strict_types=1);

namespace Cenix\RpgDice\Xwing\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RollDiceType extends AbstractType
{
    /**
     * @param mixed[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $rangeFieldOptions = [
            'attr' => [
                'class' => 'range-slider__range',
                'min' => 0,
                'max' => 5
            ],
            'help' => '0',
            'help_attr' => [
                'class' => 'range-slider__value'
            ],
            'row_attr' => ['class' => 'range-slider mb-3 p-2', 'data-action' => 'change->dice#selectDice'],
        ];

        $builder
            ->add('attack', RangeType::class, $rangeFieldOptions)
            ->add('defense', RangeType::class, $rangeFieldOptions);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
