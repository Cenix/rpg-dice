<?php

declare(strict_types=1);

namespace Genesys\Dice;

use Cenix\RpgDice\Genesys\Dice\BoostDice;
use Exception;
use PHPUnit\Framework\TestCase;

class BoostDiceTest extends TestCase
{
    private int $testRolls = 10;

    /**
     * @throws Exception
     */
    public function testRollRandomResult(): void
    {
        $dice = new BoostDice();
        $dice->roll($this->testRolls);

        $this->assertIsInt($dice->resultBlanks);
        $this->assertIsInt($dice->resultSuccesses);
        $this->assertIsInt($dice->resultAdvantages);
    }

    /**
     * @throws Exception
     */
    public function testRollBlankResult(): void
    {
        $dice = new BoostDice();
        $dice->roll($this->testRolls, 1);

        $this->assertEquals($this->testRolls, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultAdvantages);
        $this->assertEquals(0, $dice->resultSuccesses);

        $this->assertIsArray($dice->getRolledFaces());
        $this->assertEquals('boost-blank', $dice->getRolledFaces()[0]);
    }

    /**
     * @throws Exception
     */
    public function testRollAdvantageResult(): void
    {
        $dice = new BoostDice();
        $dice->roll($this->testRolls, 3);
        $this->assertEquals($this->testRolls, $dice->resultAdvantages);

        $dice = new BoostDice();
        $dice->roll($this->testRolls, 6);
        $this->assertEquals($this->testRolls * 2, $dice->resultAdvantages);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultSuccesses);
    }

    /**
     * @throws Exception
     */
    public function testRollSuccessResult(): void
    {
        $dice = new BoostDice();
        $dice->roll($this->testRolls, 4);
        $this->assertEquals($this->testRolls, $dice->resultSuccesses);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultAdvantages);
    }

    /**
     * @throws Exception
     */
    public function testRollMixedResult(): void
    {
        $dice = new BoostDice();
        $dice->roll($this->testRolls, 5);
        $this->assertEquals($this->testRolls, $dice->resultSuccesses);
        $this->assertEquals($this->testRolls, $dice->resultAdvantages);

        $this->assertEquals(0, $dice->resultBlanks);
    }
}
