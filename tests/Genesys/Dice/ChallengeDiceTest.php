<?php

declare(strict_types=1);

namespace Genesys\Dice;

use Cenix\RpgDice\Genesys\Dice\ChallengeDice;
use Exception;
use PHPUnit\Framework\TestCase;

class ChallengeDiceTest extends TestCase
{
    private int $testRolls = 10;

    /**
     * @throws Exception
     */
    public function testRollRandomResult(): void
    {
        $dice = new ChallengeDice();
        $dice->roll($this->testRolls);

        $this->assertIsInt($dice->resultBlanks);
        $this->assertIsInt($dice->resultFailures);
        $this->assertIsInt($dice->resultThreats);
        $this->assertIsInt($dice->resultDespairs);
    }

    /**
     * @throws Exception
     */
    public function testRollBlankResult(): void
    {
        $dice = new ChallengeDice();
        $dice->roll($this->testRolls, 1);

        $this->assertEquals($this->testRolls, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultThreats);
        $this->assertEquals(0, $dice->resultFailures);
        $this->assertEquals(0, $dice->resultDespairs);

        $this->assertIsArray($dice->getRolledFaces());
        $this->assertEquals('challenge-blank', $dice->getRolledFaces()[0]);
    }

    /**
     * @throws Exception
     */
    public function testRollThreatResult(): void
    {
        $dice = new ChallengeDice();
        $dice->roll($this->testRolls, 2);
        $this->assertEquals($this->testRolls, $dice->resultThreats);

        $dice = new ChallengeDice();
        $dice->roll($this->testRolls, 7);
        $this->assertEquals($this->testRolls * 2, $dice->resultThreats);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultFailures);
        $this->assertEquals(0, $dice->resultDespairs);
    }

    /**
     * @throws Exception
     */
    public function testRollFailureResult(): void
    {
        $dice = new ChallengeDice();
        $dice->roll($this->testRolls, 5);
        $this->assertEquals($this->testRolls, $dice->resultFailures);

        $dice = new ChallengeDice();
        $dice->roll($this->testRolls, 11);
        $this->assertEquals($this->testRolls * 2, $dice->resultFailures);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultThreats);
        $this->assertEquals(0, $dice->resultDespairs);
    }

    /**
     * @throws Exception
     */
    public function testRollDespairResult(): void
    {
        $dice = new ChallengeDice();
        $dice->roll($this->testRolls, 4);
        $this->assertEquals($this->testRolls, $dice->resultDespairs);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultFailures);
        $this->assertEquals(0, $dice->resultThreats);
    }

    /**
     * @throws Exception
     */
    public function testRollMixedResult(): void
    {
        $dice = new ChallengeDice();
        $dice->roll($this->testRolls, 9);
        $this->assertEquals($this->testRolls, $dice->resultFailures);
        $this->assertEquals($this->testRolls, $dice->resultThreats);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultDespairs);
    }
}
