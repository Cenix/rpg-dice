<?php

declare(strict_types=1);

namespace Genesys\Dice;

use Cenix\RpgDice\Genesys\Dice\DifficultyDice;
use Exception;
use PHPUnit\Framework\TestCase;

class DifficultyDiceTest extends TestCase
{
    private int $testRolls = 10;

    /**
     * @throws Exception
     */
    public function testRollRandomResult(): void
    {
        $dice = new DifficultyDice();
        $dice->roll($this->testRolls);

        $this->assertIsInt($dice->resultBlanks);
        $this->assertIsInt($dice->resultFailures);
        $this->assertIsInt($dice->resultThreats);
    }

    /**
     * @throws Exception
     */
    public function testRollBlankResult(): void
    {
        $dice = new DifficultyDice();
        $dice->roll($this->testRolls, 1);

        $this->assertEquals($this->testRolls, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultThreats);
        $this->assertEquals(0, $dice->resultFailures);

        $this->assertIsArray($dice->getRolledFaces());
        $this->assertEquals('difficulty-blank', $dice->getRolledFaces()[0]);
    }

    /**
     * @throws Exception
     */
    public function testRollThreatResult(): void
    {
        $dice = new DifficultyDice();
        $dice->roll($this->testRolls, 8);
        $this->assertEquals($this->testRolls, $dice->resultThreats);

        $dice = new DifficultyDice();
        $dice->roll($this->testRolls, 3);
        $this->assertEquals($this->testRolls * 2, $dice->resultThreats);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultFailures);
    }

    /**
     * @throws Exception
     */
    public function testRollFailureResult(): void
    {
        $dice = new DifficultyDice();
        $dice->roll($this->testRolls, 5);
        $this->assertEquals($this->testRolls, $dice->resultFailures);

        $dice = new DifficultyDice();
        $dice->roll($this->testRolls, 2);
        $this->assertEquals($this->testRolls * 2, $dice->resultFailures);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultThreats);
    }

    /**
     * @throws Exception
     */
    public function testRollMixedResult(): void
    {
        $dice = new DifficultyDice();
        $dice->roll($this->testRolls, 4);

        $this->assertEquals($this->testRolls, $dice->resultFailures);
        $this->assertEquals($this->testRolls, $dice->resultThreats);
        $this->assertEquals(0, $dice->resultBlanks);
    }
}
