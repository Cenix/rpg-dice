<?php

declare(strict_types=1);

namespace Genesys\Dice;

use Cenix\RpgDice\Genesys\Dice\ProficiencyDice;
use Exception;
use PHPUnit\Framework\TestCase;

class ProficienyDiceTest extends TestCase
{
    private int $testRolls = 10;

    /**
     * @throws Exception
     */
    public function testRollRandomResult(): void
    {
        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls);

        $this->assertIsInt($dice->resultBlanks);
        $this->assertIsInt($dice->resultSuccesses);
        $this->assertIsInt($dice->resultAdvantages);
        $this->assertIsInt($dice->resultTriumphs);
    }

    /**
     * @throws Exception
     */
    public function testRollBlankResult(): void
    {
        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls, 1);

        $this->assertEquals($this->testRolls, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultAdvantages);
        $this->assertEquals(0, $dice->resultSuccesses);
        $this->assertEquals(0, $dice->resultTriumphs);

        $this->assertIsArray($dice->getRolledFaces());
        $this->assertEquals('proficiency-blank', $dice->getRolledFaces()[0]);
    }

    /**
     * @throws Exception
     */
    public function testRollAdvantageResult(): void
    {
        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls, 12);
        $this->assertEquals($this->testRolls, $dice->resultAdvantages);

        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls, 7);
        $this->assertEquals($this->testRolls * 2, $dice->resultAdvantages);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultSuccesses);
        $this->assertEquals(0, $dice->resultTriumphs);
    }

    /**
     * @throws Exception
     */
    public function testRollSuccessResult(): void
    {
        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls, 11);
        $this->assertEquals($this->testRolls, $dice->resultSuccesses);

        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls, 3);
        $this->assertEquals($this->testRolls * 2, $dice->resultSuccesses);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultAdvantages);
        $this->assertEquals(0, $dice->resultTriumphs);
    }

    /**
     * @throws Exception
     */
    public function testRollTriumphResult(): void
    {
        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls, 9);
        $this->assertEquals($this->testRolls, $dice->resultTriumphs);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultSuccesses);
        $this->assertEquals(0, $dice->resultAdvantages);
    }

    /**
     * @throws Exception
     */
    public function testRollMixedResult(): void
    {
        $dice = new ProficiencyDice();
        $dice->roll($this->testRolls, 4);
        $this->assertEquals($this->testRolls, $dice->resultSuccesses);
        $this->assertEquals($this->testRolls, $dice->resultAdvantages);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultTriumphs);
    }
}
