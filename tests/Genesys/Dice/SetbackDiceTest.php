<?php

declare(strict_types=1);

namespace Genesys\Dice;

use Cenix\RpgDice\Genesys\Dice\SetbackDice;
use Exception;
use PHPUnit\Framework\TestCase;

class SetbackDiceTest extends TestCase
{
    private int $testRolls = 10;

    /**
     * @throws Exception
     */
    public function testRollRandomResult(): void
    {
        $dice = new SetbackDice();
        $dice->roll($this->testRolls);

        $this->assertIsInt($dice->resultBlanks);
        $this->assertIsInt($dice->resultFailures);
        $this->assertIsInt($dice->resultThreats);
    }

    /**
     * @throws Exception
     */
    public function testRollBlankResult(): void
    {
        $dice = new SetbackDice();
        $dice->roll($this->testRolls, 1);

        $this->assertEquals($this->testRolls, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultThreats);
        $this->assertEquals(0, $dice->resultFailures);

        $this->assertIsArray($dice->getRolledFaces());
        $this->assertEquals('setback-blank', $dice->getRolledFaces()[0]);
    }

    /**
     * @throws Exception
     */
    public function testRollThreatResult(): void
    {
        $dice = new SetbackDice();
        $dice->roll($this->testRolls, 3);

        $this->assertEquals($this->testRolls, $dice->resultThreats);
        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultFailures);
    }

    /**
     * @throws Exception
     */
    public function testRollFailureResult(): void
    {
        $dice = new SetbackDice();
        $dice->roll($this->testRolls, 5);

        $this->assertEquals($this->testRolls, $dice->resultFailures);
        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultThreats);
    }
}
