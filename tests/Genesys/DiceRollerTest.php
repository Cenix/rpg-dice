<?php

declare(strict_types=1);

namespace Genesys;

use Cenix\RpgDice\Genesys\DiceRoller;
use PHPUnit\Framework\TestCase;

use function in_array;

class DiceRollerTest extends TestCase
{
    private int $testRolls = 10;

    public function testGetResultText(): void
    {
        $diceRoller = new DiceRoller();
        $diceRoller->setAbility($this->testRolls);
        $diceRoller->rollDicePool();
        $this->assertIsString($diceRoller->getResultText());

        $diceRoller = new DiceRoller();
        $diceRoller->rollDicePool();
        $this->assertEquals($diceRoller->getResultText(), '*Wash*');
    }

    public function testGetResultSymbols(): void
    {
        $diceRoller = new DiceRoller();
        $diceRoller->setAbility($this->testRolls);
        $diceRoller->rollDicePool();
        $this->assertIsArray($diceRoller->getResultSymbols());

        $this->assertTrue(in_array('success', $diceRoller->getResultSymbols()));
        $this->assertTrue(in_array('advantage', $diceRoller->getResultSymbols()));
    }

    public function testGetAutoSymbols(): void
    {
        $diceRoller = new DiceRoller();
        $diceRoller->setAbility($this->testRolls);
        $diceRoller->setAutoAdvantage(1);
        $diceRoller->rollDicePool();
        $this->assertIsArray($diceRoller->getAutoSymbols());
        $this->assertTrue(in_array('advantage', $diceRoller->getAutoSymbols()));
    }

    public function testRollDicePool(): void
    {
        $diceRoller = new DiceRoller();
        $diceRoller->setAbility($this->testRolls);
        $diceRoller->setProficiency($this->testRolls);
        $diceRoller->setBoost($this->testRolls);
        $diceRoller->setDifficulty($this->testRolls);
        $diceRoller->setChallenge($this->testRolls);
        $diceRoller->setSetback($this->testRolls);

        $diceRoller->rollDicePool();

        $this->assertIsInt($diceRoller->getSuccesses('net'));
        $this->assertIsInt($diceRoller->getSuccesses('gross'));

        $this->assertIsInt($diceRoller->getAdvantages('net'));
        $this->assertIsInt($diceRoller->getAdvantages('gross'));

        $this->assertIsInt($diceRoller->getTriumphs('net'));
        $this->assertIsInt($diceRoller->getTriumphs('gross'));

        $this->assertIsInt($diceRoller->getFailures('net'));
        $this->assertIsInt($diceRoller->getFailures('gross'));

        $this->assertIsInt($diceRoller->getThreats('net'));
        $this->assertIsInt($diceRoller->getThreats('gross'));

        $this->assertIsInt($diceRoller->getDespairs('net'));
        $this->assertIsInt($diceRoller->getDespairs('gross'));

        $this->assertIsInt($diceRoller->getBlanks('net'));
        $this->assertIsInt($diceRoller->getBlanks('gross'));
    }

    public function testGetRolledFaces(): void
    {
        $diceRoller = new DiceRoller();
        $diceRoller->setAbility($this->testRolls);
        $diceRoller->rollDicePool();
        $this->assertIsArray($diceRoller->getRolledFaces());

        $this->assertStringContainsString('ability-', (string)$diceRoller->getRolledFaces()[0]);
    }
}
