<?php

declare(strict_types=1);

namespace Genesys\Form;

use Cenix\RpgDice\Genesys\DiceRoller;
use Cenix\RpgDice\Genesys\Form\RollDiceType;
use Symfony\Component\Form\Test\TypeTestCase;

class RollDiceTypeTest extends TypeTestCase
{
    private int $testRolls = 10;

    public function testBuildForm(): void
    {
        $diceRoller = new DiceRoller();

        $formData = [
            'ability' => $this->testRolls,
            'proficiency' => $this->testRolls,
            'boost' => $this->testRolls,
            'difficulty' => $this->testRolls,
            'challenge' => $this->testRolls,
            'setback' => $this->testRolls,
            'autoAdvantages' => $this->testRolls,
            'autoThreats' => $this->testRolls,
            'autoSuccesses' => $this->testRolls,
            'autoFailures' => $this->testRolls,
        ];

        $form = $this->factory->create(RollDiceType::class, $diceRoller);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        $expectedDiceRoller = new DiceRoller();
        $expectedDiceRoller->setAbility($this->testRolls);
        $expectedDiceRoller->setProficiency($this->testRolls);
        $expectedDiceRoller->setBoost($this->testRolls);
        $expectedDiceRoller->setDifficulty($this->testRolls);
        $expectedDiceRoller->setChallenge($this->testRolls);
        $expectedDiceRoller->setSetback($this->testRolls);
        $expectedDiceRoller->setAutoAdvantage($this->testRolls);
        $expectedDiceRoller->setAutoThreat($this->testRolls);
        $expectedDiceRoller->setAutoSuccess($this->testRolls);
        $expectedDiceRoller->setAutoFail($this->testRolls);

        $this->assertEquals($expectedDiceRoller, $diceRoller);
    }
}
