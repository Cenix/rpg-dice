<?php

declare(strict_types=1);

namespace Xwing\Dice;

use Cenix\RpgDice\Xwing\Dice\AttackDice;
use Exception;
use PHPUnit\Framework\TestCase;

class AttackDiceTest extends TestCase
{
    private int $testRolls = 10;

    /**
     * @throws Exception
     */
    public function testRollRandomResult(): void
    {
        $dice = new AttackDice();

        $dice->roll($this->testRolls);

        $this->assertIsInt($dice->resultBlanks);
        $this->assertIsInt($dice->resultHits);
        $this->assertIsInt($dice->resultFocuses);
        $this->assertIsInt($dice->resultCriticals);
    }

    /**
     * @throws Exception
     */
    public function testRollBlankResult(): void
    {
        $dice = new AttackDice();

        $dice->roll($this->testRolls, 1);

        $this->assertIsArray($dice->getRolledFaces());
        $this->assertEquals('attack-blank', $dice->getRolledFaces()[0]);

        $this->assertEquals($this->testRolls, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultHits);
        $this->assertEquals(0, $dice->resultFocuses);
        $this->assertEquals(0, $dice->resultCriticals);
    }

    /**
     * @throws Exception
     */
    public function testRollHitWithFocusResult(): void
    {
        $dice = new AttackDice();

        $dice->roll($this->testRolls, 4);
        $dice->roll($this->testRolls, 7);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals($this->testRolls, $dice->resultHits);
        $this->assertEquals($this->testRolls, $dice->resultFocuses);
        $this->assertEquals(0, $dice->resultCriticals);
    }

    /**
     * @throws Exception
     */
    public function testRollCriticalResult(): void
    {
        $dice = new AttackDice();

        $dice->roll($this->testRolls, 3);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultHits);
        $this->assertEquals(0, $dice->resultFocuses);
        $this->assertEquals($this->testRolls, $dice->resultCriticals);
    }
}
