<?php

declare(strict_types=1);

namespace Xwing\Dice;

use Cenix\RpgDice\Xwing\Dice\DefenseDice;
use Exception;
use PHPUnit\Framework\TestCase;

class DefenseDiceTest extends TestCase
{
    private int $testRolls = 10;

    /**
     * @throws Exception
     */
    public function testRollRandomResult(): void
    {
        $dice = new DefenseDice();

        $dice->roll($this->testRolls);

        $this->assertIsInt($dice->resultBlanks);
        $this->assertIsInt($dice->resultEvades);
        $this->assertIsInt($dice->resultFocuses);
    }

    /**
     * @throws Exception
     */
    public function testRollBlankResult(): void
    {
        $dice = new DefenseDice();

        $dice->roll($this->testRolls, 1);

        $this->assertIsArray($dice->getRolledFaces());
        $this->assertEquals('defense-blank', $dice->getRolledFaces()[0]);

        $this->assertEquals($this->testRolls, $dice->resultBlanks);
        $this->assertEquals(0, $dice->resultEvades);
        $this->assertEquals(0, $dice->resultFocuses);
    }

    /**
     * @throws Exception
     */
    public function testRollEvadeWithFocusResult(): void
    {
        $dice = new DefenseDice();

        $dice->roll($this->testRolls, 4);
        $dice->roll($this->testRolls, 6);

        $this->assertEquals(0, $dice->resultBlanks);
        $this->assertEquals($this->testRolls, $dice->resultEvades);
        $this->assertEquals($this->testRolls, $dice->resultFocuses);
    }
}
